import json
import os
from typing import Generator

import requests
from detect_cube_configuration import detect_cube_configuration


def validate_directory(folder_path) -> Generator[bool, None, None]:
    for root, dirs, files in os.walk(folder_path):
        video_files = [file for file in files if file.endswith(".mp4")]

        for video_file in video_files:
            video_number = video_file.split("_")[-1].split(".")[0]
            config_file = f"config{video_number}.json"
            config_path = os.path.join(root, config_file)
            if os.path.exists(config_path):
                yield validate_result_fs(os.path.join(root, video_file), config_path)
            else:
                print(f"Warning: Configuration file {config_file} not found for video {video_file}")


def validate_result_fs(video_path, config_json_filepath) -> bool:
    with open(config_json_filepath, 'r') as file:
        expected_config = json.load(file).get('config', {})
        detected_config = detect_cube_configuration(video_path)
        print(f"Detected config: {detected_config.get('config', {})}")
        print(f"Expected config: {expected_config}")
        print("Matching result:")
        if detected_config.get('config', {}) != expected_config:
            return False
        return True


def validate_result_api(video_path, validate_url) -> bool:
    detected_config = detect_cube_configuration(video_path)
    resp = requests.post(url=validate_url, json=detected_config)
    return True if resp.status_code == 200 else False
