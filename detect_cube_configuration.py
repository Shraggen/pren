import sys
import time
import datetime
import cv2
import numpy as np

from Constants import ROI_COORDINATES_TURNTABLE, COLOR_RANGES_TURNTABLE, ROI_COORDINATES_CUBE, ROI_ORDER_MAP_CUBE, \
    COLOR_RANGES_CUBE
from CubeOrientation import CubeOrientation


def detect_cube_configuration(ip_address, username, password, profile):
    url = f'rtsp://{username}:{password}@{ip_address}/axis-media/media.amp?streamprofile={profile}'
    cap = cv2.VideoCapture(url)

    bottom_left_poi = ROI_COORDINATES_TURNTABLE["bottom_left"]
    bottom_right_poi = ROI_COORDINATES_TURNTABLE["bottom_right"]
    top_left_poi = ROI_COORDINATES_TURNTABLE["top_left"]
    top_right_poi = ROI_COORDINATES_TURNTABLE["top_right"]

    first_config = None
    first_orientation = None

    initial_delay = 1
    time.sleep(initial_delay)

    if cap is None or not cap.isOpened():
        print('Warning: unable to open video source: ', ip_address)
        return None
    while True:
        ret, frame = cap.read()

        bottom_left_roi = extract_roi(frame, bottom_left_poi)
        bottom_right_roi = extract_roi(frame, bottom_right_poi)
        top_left_roi = extract_roi(frame, top_left_poi)
        top_right_roi = extract_roi(frame, top_right_poi)

        lower, upper = COLOR_RANGES_TURNTABLE

        bottom_left_detected = np.any(cv2.inRange(bottom_left_roi, lower, upper))
        bottom_right_detected = np.any(cv2.inRange(bottom_right_roi, lower, upper))
        top_left_detected = np.any(cv2.inRange(top_left_roi, lower, upper))
        top_right_detected = np.any(cv2.inRange(top_right_roi, lower, upper))

        current_configuration = determine_cube_orientation(bottom_left_detected, bottom_right_detected,
                                                           top_left_detected, top_right_detected)

        # DEMO
        visualize_rois(frame, bottom_left_detected,
                       bottom_right_detected, top_left_detected, top_right_detected)

        if current_configuration is CubeOrientation.IN_BETWEEN:
            continue

        if (first_config and first_orientation) is None:
            first_config = detect_frame_configuration(frame, current_configuration)
            first_orientation = current_configuration
            continue
        if first_orientation == current_configuration:
            continue
        if first_orientation is not current_configuration:
            cap.release()
            return merge_configs(first_config, detect_frame_configuration(frame, current_configuration))


def merge_configs(first_config, second_config):
    return {
        "time": datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S"),
        "config": {str(k): v for k, v in
                   sorted({**first_config["config"], **second_config["config"]}.items())}
    }


def determine_cube_orientation(bottom_left_detected, bottom_right_detected, top_left_detected, top_right_detected):
    if top_left_detected and bottom_left_detected:
        return CubeOrientation.FACING_LEFT
    elif top_left_detected and top_right_detected:
        return CubeOrientation.FACING_AWAY
    elif top_right_detected and bottom_right_detected:
        return CubeOrientation.FACING_RIGHT
    elif bottom_left_detected and bottom_right_detected:
        return CubeOrientation.FACING_FORWARD
    else:
        return CubeOrientation.IN_BETWEEN


def detect_frame_configuration(img, cube_orientation: CubeOrientation):
    hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    config = {"config": {}}

    roi_list_and_order = get_roi_list_with_order(cube_orientation)
    roi_list, order = roi_list_and_order

    for cube_number, roi in zip(order, roi_list):
        roi_image = extract_roi(hsv_img, roi)
        dominant_color = detect_color(roi_image)
        config["config"][str(cube_number)] = dominant_color

    return config


def detect_color(roi_image):
    max_color_count = 0
    dominant_color = ""

    for color, (lower, upper) in COLOR_RANGES_CUBE.items():
        lower = np.array(lower)
        upper = np.array(upper)

        color_mask = cv2.inRange(roi_image, lower, upper)
        color_count = np.sum(color_mask) / 255.0

        if color_count > max_color_count:
            max_color_count = color_count
            dominant_color = color

    return dominant_color


def get_roi_list_with_order(cube_orientation: CubeOrientation):
    roi_list = [ROI_COORDINATES_CUBE["bottom_right"], ROI_COORDINATES_CUBE["bottom_left"],
                ROI_COORDINATES_CUBE["top_right"], ROI_COORDINATES_CUBE["top_left"]]
    return roi_list, ROI_ORDER_MAP_CUBE.get(cube_orientation, None)


def extract_roi(image, roi):
    x, y, w, h = roi
    return image[y:y + h, x:x + w]


# Function to visualize the ROIs using cv2.imshow
def visualize_rois(frame, bottom_left_detected, bottom_right_detected, top_left_detected, top_right_detected):
    # Convert the Turntable ROI start and end coordinates to tuples
    bottom_left_start_turntable = tuple(ROI_COORDINATES_TURNTABLE["bottom_left"][:2])
    bottom_left_end_turntable = tuple(
        np.array(ROI_COORDINATES_TURNTABLE["bottom_left"][:2]) + np.array(ROI_COORDINATES_TURNTABLE["bottom_left"][2:]))

    bottom_right_start_turntable = tuple(ROI_COORDINATES_TURNTABLE["bottom_right"][:2])
    bottom_right_end_turntable = tuple(np.array(ROI_COORDINATES_TURNTABLE["bottom_right"][:2]) + np.array(
        ROI_COORDINATES_TURNTABLE["bottom_right"][2:]))

    top_left_start_turntable = tuple(ROI_COORDINATES_TURNTABLE["top_left"][:2])
    top_left_end_turntable = tuple(
        np.array(ROI_COORDINATES_TURNTABLE["top_left"][:2]) + np.array(ROI_COORDINATES_TURNTABLE["top_left"][2:]))

    top_right_start_turntable = tuple(ROI_COORDINATES_TURNTABLE["top_right"][:2])
    top_right_end_turntable = tuple(
        np.array(ROI_COORDINATES_TURNTABLE["top_right"][:2]) + np.array(ROI_COORDINATES_TURNTABLE["top_right"][2:]))

    # Convert the Cube ROI start and end coordinates to tuples
    bottom_left_start_cube = tuple(ROI_COORDINATES_CUBE["bottom_left"][:2])
    bottom_left_end_cube = tuple(
        np.array(ROI_COORDINATES_CUBE["bottom_left"][:2]) + np.array(ROI_COORDINATES_CUBE["bottom_left"][2:]))

    bottom_right_start_cube = tuple(ROI_COORDINATES_CUBE["bottom_right"][:2])
    bottom_right_end_cube = tuple(np.array(ROI_COORDINATES_CUBE["bottom_right"][:2]) + np.array(
        ROI_COORDINATES_CUBE["bottom_right"][2:]))

    top_left_start_cube = tuple(ROI_COORDINATES_CUBE["top_left"][:2])
    top_left_end_cube = tuple(
        np.array(ROI_COORDINATES_CUBE["top_left"][:2]) + np.array(ROI_COORDINATES_CUBE["top_left"][2:]))

    top_right_start_cube = tuple(ROI_COORDINATES_CUBE["top_right"][:2])
    top_right_end_cube = tuple(
        np.array(ROI_COORDINATES_CUBE["top_right"][:2]) + np.array(ROI_COORDINATES_CUBE["top_right"][2:]))

    # Color coding based on detection status
    color_bottom_left_turntable = (0, 255, 0) if bottom_left_detected else (0, 0, 255)
    color_bottom_right_turntable = (0, 255, 0) if bottom_right_detected else (0, 0, 255)
    color_top_left_turntable = (0, 255, 0) if top_left_detected else (0, 0, 255)
    color_top_right_turntable = (0, 255, 0) if top_right_detected else (0, 0, 255)

    # Draw rectangles on the frame to visualize the ROIs for Turntable
    cv2.rectangle(frame, bottom_left_start_turntable, bottom_left_end_turntable, color_bottom_left_turntable, 2)
    cv2.rectangle(frame, bottom_right_start_turntable, bottom_right_end_turntable, color_bottom_right_turntable, 2)
    cv2.rectangle(frame, top_left_start_turntable, top_left_end_turntable, color_top_left_turntable, 2)
    cv2.rectangle(frame, top_right_start_turntable, top_right_end_turntable, color_top_right_turntable, 2)

    # Draw rectangles on the frame to visualize the ROIs for Cube
    cv2.rectangle(frame, bottom_left_start_cube, bottom_left_end_cube, (0, 255, 0), 2)
    cv2.rectangle(frame, bottom_right_start_cube, bottom_right_end_cube, (0, 255, 0), 2)
    cv2.rectangle(frame, top_left_start_cube, top_left_end_cube, (0, 255, 0), 2)
    cv2.rectangle(frame, top_right_start_cube, top_right_end_cube, (0, 255, 0), 2)

    # Resize the display window
    cv2.namedWindow('Frame with ROIs', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Frame with ROIs', 1200, 900)  # Adjust width and height as needed

    # Display the frame with ROIs
    cv2.imshow('Frame with ROIs', frame)

    # Wait for a key press
    key = cv2.waitKey(1)

    # Check for key events
    if key == ord('p'):
        cv2.waitKey(0)  # Pause until any key is pressed
    elif key == ord('q'):
        cv2.destroyAllWindows()
        sys.exit(0)
