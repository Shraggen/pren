from enum import Enum


class CubeOrientation(Enum):
    FACING_FORWARD = 0
    FACING_LEFT = 1
    FACING_AWAY = 2
    FACING_RIGHT = 3
    IN_BETWEEN = 4
