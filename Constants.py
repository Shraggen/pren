import colorsys
from CubeOrientation import CubeOrientation

# Constants Cube
RED_RGB = (138, 4, 11)
LOWER_RED_HUE = max(0, int(colorsys.rgb_to_hsv(*RED_RGB)[0] * 180) - 10)
UPPER_RED_HUE = min(180, int(colorsys.rgb_to_hsv(*RED_RGB)[0] * 180) + 10)

COLOR_RANGES_CUBE = {
    "red": ([LOWER_RED_HUE, 50, 50], [UPPER_RED_HUE, 255, 255]),
    "blue": ([80, 100, 100], [130, 255, 255]),
    "yellow": ([20, 50, 100], [40, 255, 255])
}

ROI_COORDINATES_CUBE = {
    "bottom_right": (730, 250, 20, 20),
    "bottom_left": (530, 250, 20, 20),
    "top_right": (730, 150, 20, 20),
    "top_left": (530, 150, 20, 20)
}

ROI_ORDER_MAP_CUBE = {
    CubeOrientation.FACING_FORWARD: [2, 4, 6, 8],
    CubeOrientation.FACING_LEFT: [3, 1, 7, 5],
    CubeOrientation.FACING_AWAY: [4, 2, 8, 6],
    CubeOrientation.FACING_RIGHT: [1, 3, 5, 7]
}

# Constants Turntable
ROI_COORDINATES_TURNTABLE = {
    "bottom_right": (875, 425, 20, 20),
    "bottom_left": (390, 420, 20, 20),
    "top_right": (825, 145, 20, 20),
    "top_left": (425, 145, 20, 20)
}

COLOR_RANGES_TURNTABLE = ((0, 190, 150), (255, 255, 255))
